INTRODUCTION
------------
The Symfony validation translator module translates strings
from the Symfony validation constraint messages.

-   For a full description of the module, visit the project page:
    https://www.drupal.org/project/symfony_validator_translator
-   To submit bug reports and feature suggestions, or track changes:
    https://www.drupal.org/project/issues/symfony_validator_translator

REQUIREMENTS
------------
The module requires the symfony component to be installed:
"symfony/translation": "~3.4.0"
This will be automatically installed when you install using composer
(recommended).

INSTALLATION
------------
As this module requires a symfony component, it is highly recommended
to use composer to install it. See
https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies
for more information. Although you may be able to install the
module in another way, that is not supported.

CONFIGURATION
-------------
The module has no menu or modifiable settings.

### Overriding Symfony translations
Sometimes you may need to override the Symfony translation.
In that case just copy the original message from the Symfony
class (or find it in the Symfony translation file) and add it to your
translation file as the `"msgid"` for that particular language whose
translation you are overriding into your custom module.

Example:
Let's assume you want to override the original translated
string of the Dutch language of a string used in the Symfony IBAN
constraint.. The original translated Dutch string is `Dit is geen geldig
internationaal bankrekeningnummer (IBAN).` You would add this to a .po
file that you can import into Drupal whichever way you prefer.

```
msgid "This is not a valid International Bank Account Number (IBAN)."
msgstr "Dit is geen geldig IBAN nummer."
```

Alternatively, you could add this string pair in Interface Translation
(/admin/config/regional/translate).

Now the error message that will be shown will be your custom
translation. To revert back to the Symfony translation,
just delete the custom translation, update the Drupal translations
and you will then see the original Symfony translation.

TROUBLESHOOTING
---------------
There are modules that will make your application break once
you enable this module. This is because those modules are type coupling
their dependencies instead of coding against an interface. That type of
programming approach leads to the application breaking when a decorator
pattern is used (or when the service is replaced completely).

Below are known modules that are type coupling their dependencies.
-   [Entity clone](https://www.drupal.org/project/entity_clone)
-   [Entity Browser](https://www.drupal.org/project/entity_browser)
    (Issue: [#3100254: Typehint interface in stead of concrete
    class](https://www.drupal.org/node/3100254))

## Solution
You will need to patch those modules by changing their dependencies to
type hint an interface instead of a concrete or implemented class. In
this case the above modules make PHP throw a TypeError caused by them
expecting the concrete class
`Drupal\Core\StringTranslation\TranslationManager` instead of the
interface `Drupal\Core\StringTranslation\TranslationInterface` in their
constructors.

Change their constructor (or other method of injection) to type hint
the above interface instead of the implemented class.

Example:
In `Drupal\entity_browser\Permissions`
The constructor looks like this:
```
  /**
   * Constructs Permissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity manager service.
   * @param \Drupal\Core\StringTranslation\TranslationManager $translation
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
TranslationManager $translation) {
```
It would have to be changed to:
```
  /**
   * Constructs Permissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity manager service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
TranslationInterface $translation) {
```

Take note of the `TranslationManager` changing to `TranslationInterface`.
As noted, there is actually an issue for this particular change:
[#3100254: Typehint interface in stead of concrete
 class](https://www.drupal.org/node/3100254)

**Note**
There are more contributed modules you may be using that are not
following best practices with regard to this. If you notice any other
modules doing this, please file an issue in their issue queue. You can
use the explanation above to create a patch. You may also create an
issue in this module's issue queue to highlight the problem. Please link
to the issue in the other module's issue queue, in that case.

FAQ
---
Q: I enabled the module and the Symfony translation component is missing.
A: Use composer to add the component.

MAINTAINERS
-----------
Current maintainers:
- Bonaventure Wani (bonrita) - https://www.drupal.org/user/415370
